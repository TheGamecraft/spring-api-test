package com.example.accessingdatamongodb;

import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;
import java.util.Optional;

@RestController
public class CustomerController {

    @Autowired
    private CustomerRepository repository;

    @GetMapping("/customer")
    public List<Customer> customer() {
        return repository.findAll();
    }

    @GetMapping("/customer/{id}")
    public Optional<Customer> customer(@PathVariable("id") String id) {
        return repository.findById(id);
    }

    @PostMapping("/customer")
    public Customer newCustomer(@RequestBody Customer newCustomer) {
        return repository.save(newCustomer);
    }

    @PutMapping("/customer/{id}")
    public Customer updateCustomer(@PathVariable("id") String id,@RequestBody Customer updatedCustomer) throws Exception {
        return repository.findById(id).map(customer -> {
            customer.setFirstName(updatedCustomer.firstName);
            customer.setLastName(updatedCustomer.lastName);
            return repository.save(customer);
        }).orElseThrow(Exception::new);
    }

    @DeleteMapping("/customer/{id}")
    void deleteCustomer(@PathVariable("id")String id) {
        repository.deleteById(id);
    }
}
