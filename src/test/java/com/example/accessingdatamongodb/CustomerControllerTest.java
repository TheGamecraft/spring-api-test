package com.example.accessingdatamongodb;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
public class CustomerControllerTest {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private MockMvc mockMvc;

    public void beforeTestCreateCustomer() {
        customerRepository.save(new Customer("John Alexander Maximus","Doe"));
    }

    public void afterTestDeleteCustomer(String firstName) {
        Customer customer = customerRepository.findByFirstName(firstName);
        if (customer != null) {
            customerRepository.delete(customer);
        }
    }

    @Test
    public void shouldCreateNewCustomer() throws Exception {
        this.mockMvc.perform(
                post("/customer")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"firstName\": \"John Alexander Maximus Samwise\",\"lastName\": \"Gamgee\"}")
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());

        afterTestDeleteCustomer("John Alexander Maximus Samwise");
    }

    @Test
    public void shouldUpdateCustomer() throws Exception {
        beforeTestCreateCustomer();
        Customer customer = customerRepository.findByFirstName("John Alexander Maximus");

        this.mockMvc.perform(
                put("/customer/"+customer.id)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"firstName\": \"John Alexander Maximus Samwise\",\"lastName\": \"Doe\"}")
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());

        afterTestDeleteCustomer("John Alexander Maximus Samwise");
    }

    @Test
    public void shouldGetCustomer() throws Exception {
        beforeTestCreateCustomer();
        Customer customer = customerRepository.findByFirstName("John Alexander Maximus");

        this.mockMvc.perform(
                get("/customer/"+customer.id)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(customer.id))
                .andExpect(jsonPath("$.firstName").value(customer.firstName))
                .andExpect(jsonPath("$.lastName").value(customer.lastName));
        afterTestDeleteCustomer("John Alexander Maximus");
    }

    @Test
    public void shouldDeleteCustomer() throws Exception {
        beforeTestCreateCustomer();
        Customer customer = customerRepository.findByFirstName("John Alexander Maximus");

        this.mockMvc.perform(
                delete("/customer/"+customer.id))
                .andExpect(status().isOk());

        afterTestDeleteCustomer("John Alexander Maximus");
    }

    @Test
    public void shouldGetAllCustomer() throws Exception {
        beforeTestCreateCustomer();
        List<Customer> customers = customerRepository.findAll();

        this.mockMvc.perform(
                get("/customer"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$",hasSize(customers.size())));
        afterTestDeleteCustomer("John Alexander Maximus");
    }
}
